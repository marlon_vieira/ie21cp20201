![](Utfpr.gif)
# Introdução

O presente trabalho tem como finalidade pesquisar, discutir, elaborar, desenvolver e publicar um projeto em grupo. O qual consiste em criar um esquema elétrico para realizar determinadas tarefas, as quais foram discutidas pela equipe. Para a criação do mesmo foi utilizado ferramentas online gratuitas e vídeo aulas disponibilizadas na plataforma Moodle.

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Marlon Vieira| [@marlon_vieira](https://gitlab.com/marlon_vieira)|
|Lucas Cardoso| [@Luc4sC4r0s0](https://gitlab.com/Luc4sC4r0s0)|
|Vitor Alex Basso| [@vitoralexbasso53](https://gitlab.com/vitoralexbasso53) |
|Mateus Silva| [@eigenstirner](https://gitlab.com/eigenstirner) |

# Documentação

A documentação do projeto pode ser acessada pelo link:

> * [GitLab](https://marlon_vieira.gitlab.io/ie21cp20201/)
> * [GitHub](https://marlonvieira.github.io/Abajeto/)

# Links Úteis

* [GitLab](https://marlonvieira.github.io/Abajeto/)
* [GitHub](https://marlonvieira.github.io/ProjetoAbajur/)
* [StackEdit](https://stackedit.io/)
* [Tinkercad](https://www.tinkercad.com/dashboard)
* [Fritzing](https://fritzing.org/)
* [Gravador de tela](https://www.apowersoft.com.br/gravador-de-tela-gratis)
