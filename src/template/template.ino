//Criando duas constantes para poupar memória
define POT A2; // Constante para indicar a entrada analógica do potenciômetro
define LAMP 13; // Consstante para indicar a entrada digital da lampada
int valor = 0;

void setup()
{
    Serial.begin(9600); 
    pinMode(POT, INPUT);
    pinMode(LAMP, OUTPUT);
}

void loop()
{
  valor = analogRead(POT); // Variavel recebe o valor do potenciômetro
  if(valor < 250)
  {
    digitalWrite(LAMP, LOW); // Desliga a lampada caso o potenciometro esteja abaixo de 250
    delay(800); // Delay de 0.8 segundos
  }  
  else
  {
    digitalWrite(LAMP, HIGH); // Liga a lampada caso contrário
    delay(800); // Delay de 0.8 segundos
  }
  Serial.println(valor);
}